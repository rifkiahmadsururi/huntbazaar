<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class UndanganController extends Controller
{

    public function index()
    {
        $vw_undanganRegristrasi = \App\Models\vw_undanganRegristrasi::All();

        return view('undangan.index', ['Undangan' => $vw_undanganRegristrasi]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $save = new \App\Models\Undangan;
        $email = $request->get('email');

        $save->email = $email;
        $save->undangan_status_id = 1;
        $save->save();

        $lastID = $save->id;
        $undangan = \App\Models\Undangan::find($lastID);
        $link = sha1(md5($lastID) . md5($email));
        $undangan->link_undangan = sha1(md5($lastID) . md5($email));
        $undangan->save();

        $link = "http://" . $_SERVER['SERVER_NAME'] . "/registrasi/" . $email . "/" . $link;
        $body   = "Selamat, anda terpilih untuk mendapatkan spesial diskon di even kami, silahkan daftar pada link ini " . $link . "<br><br>Selamat Berbelanja";


        $details['dest'] = $email;
        $details['body'] = $body;

        dispatch(new \App\Jobs\sendUndangan($details));
        return redirect('/undangan');
    }

    public function registrasi(Request $request, $email, $link)
    {

        $MAX_DAFTAR = env("MAX_DAFTAR");

        $undangan = DB::table('undangans')
            ->where('email', '=', $email)
            ->where('link_undangan', '=', $link)
            ->get();

        foreach ($undangan as $row) {
            $email = $row->email;
            $kode_registrasi = $row->kode_registrasi;
        }
        if ($kode_registrasi == null) {
            $designer = \App\Models\designer::All();
            return view('undangan.landingPage.index')
                ->with([
                    'email' => $email,
                    'designer' => $designer,
                    'MAX_DAFTAR' => $MAX_DAFTAR
                ]);;
        } else {
            return "Anda telah registrasi, berikut kode registrasi anda <br>" . $kode_registrasi;
        }
    }

    public function registrasiSimpan(Request $request, $email, $link)
    {
        $undangan = DB::table('undangans')
            ->where('email', '=', $email)
            ->where('link_undangan', '=', $link)
            ->whereNull('kode_registrasi')
            ->get();
        $id = 0;
        foreach ($undangan as $row) {
            $id = $row->id;
            $kode_registrasi = $row->kode_registrasi;
        }
        $MAX_DAFTAR = env("MAX_DAFTAR");
        //echo date("Y-m-d") . "<br>";
        //echo $MAX_DAFTAR;

        if (date("Y-m-d") < $MAX_DAFTAR) {

            //echo "<br>" . $id;

            if ($id > 0) {
                $undangan = \App\Models\Undangan::find($id);


                $undangan->nama = $request->get('nama');

                $undangan->jenis_kelamin = $request->get('jk');
                $undangan->tanggal_lahir = $request->get('tanggalLahir');

                $nama_ds = "";
                $designer = $request->get('designer');
                foreach ($designer as $ds) {
                    $nama_ds = $nama_ds . "," . $ds;
                }

                $undangan->designer = $nama_ds;
                $undangan->kode_registrasi = time();
                $undangan->save();

                $details['dest'] = $email;
                $details['body'] = "Terimakasih";

                dispatch(new \App\Jobs\SendEmailThankYou($details))->delay(3600);

                return "Thank you, Data anda telah tersimpan di database kami. berikut kode registrasi anda <br>" . $kode_registrasi;
            } else {
                $link = "/registrasi/" . $email . "/" . $link;
                return redirect($link);
            }
        } else {
            return "Anda terlambat Mendaftar";
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
