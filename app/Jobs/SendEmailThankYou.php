<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class SendEmailThankYou implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $details;
    public function __construct($details)
    {
        $this->details = $details;
    }

    public function handle()
    {
        //$email = new emailThankYou();
        //Mail::to($this->details['dest'])->send($email);

        $body   = $this->details['body'];
        $details = [
            'body' => $body
        ];
        Mail::to($this->details['dest'])->send(new \App\Mail\emailThankYou($details));
    }
}
