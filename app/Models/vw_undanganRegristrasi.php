<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class vw_undanganRegristrasi extends Model
{
    use HasFactory;

    public $table = "vw_undanganRegristrasi";
}
