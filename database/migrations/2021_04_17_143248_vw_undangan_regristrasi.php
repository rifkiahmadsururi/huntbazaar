<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class VwUndanganRegristrasi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        //$query = "";
        \DB::statement($this->createView());
    }

    private function createView(): string
    {
        return <<< SQL
            create view vw_undanganRegristrasi as 
            select 
                id, 
                email, 
                    case 
                        when nama is null then 'Belum Regristrasi' 
                        else 'sudah regristrasi' 
                    end as status_regristrasi, 
                    case when nama is null then 0 else 1 end as flag_regristrasi ,
                    link_undangan,
                    undangan_status_id
                    from undangans;
            SQL;
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    private function dropView(): string
    {
        return <<<SQL

            DROP VIEW IF EXISTS `view_user_data`;
            SQL;
    }
}
