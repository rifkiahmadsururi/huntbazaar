<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\designer;

class designerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        designer::create([
            'nama' => 'A Bathing Ape'
        ]);

        designer::create([
            'nama' => 'A.W.A.K.E'
        ]);
    }
}
