@extends('layouts.app')
@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Data Undangan</h1>
</div>
<hr>
<div class="card-header py-3" align="right">
    <button class="btn btn-primary btn-sm btn-flat" data-toggle="modal" data-target="#modal-add"><i class="fa fa-plus"></i>Tambah</button>
</div>
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table tablebordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr align="center">
                        <th width="5%">Email</th>
                        <th width="25%">Status</th>
                        <th width="25%">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($Undangan as $row)
                    <tr>
                        <td>{{$row->email}}</td>

                        <?php
                        if ($row->flag_regristrasi == 0) {
                            echo "<td><span style='background-color:yellow;'>";
                        } else {
                            echo "<td><span style='background-color:green;'>";
                        }
                        ?>
                        {{$row->status_regristrasi}}
                        </span>
                        </td>
                        <td>
                            <?php
                            if ($row->flag_regristrasi == 0) {
                            ?>
                                <button class="btn btn-primary" onclick="kirimUndangan('{{$row->email}}','/{{$row->email}}/{{$row->link_undangan}}')">Kirim ulang undangan</button>
                            <?php
                            }
                            ?>
                        </td>

                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- modal add data-->
<div class="modal inmodal fade" id="modal-add" tabindex="-1" role="dialog" ariahidden="true">
    <div class="modal-dialog modal-xs">
        <form name="frm_add" id="frm_add" class="formhorizontal" action="#" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Tambah Data User</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-lg-20 controllabel">ALamat Email</label>
                        <div class="col-lg10">
                            <input type="email" name="email" required class="form-control">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" datadismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                    @endsection
                </div>
            </div>
        </form>
    </div>
</div>
</div>
<script>
    function kirimUndangan(email, link) {
        var base_url = window.location.origin + '/registrasi';
        var linkEmail = "../send-email?dest=" + email + "&title=Huntbazzar&body=silahkan kik link ini untuk mendapatkan promo menarik <a href='" + base_url + link + "'>Daftar<a/><br>atau menuju link";
        console.log(linkEmail);
        fetch(linkEmail)
            .then(alert('kirim undangan sukses'))
            .then(json => console.log(json))
    }
</script>