<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <meta name="description" content="">
  <meta name="author" content="">


  <title>Starter Template for Bootstrap</title>

  <!-- Bootstrap core CSS -->
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  <link href="https://maxcdn.bootstrapcdn.com/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="" rel="stylesheet">
  <link href="{{ asset('landing/starter-template.css')}}" rel="stylesheet">

  <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
  <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
  <script src="../../assets/js/ie-emulation-modes-warning.js"></script>

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="#">HUNTBAZAAR</a>
      </div>
      <div id="navbar" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
          <li class="active"><a href="#">Home</a></li>
          <li><a href="#">About</a></li>
          <li><a href="#">Contact</a></li>
        </ul>
      </div>
      <!--/.nav-collapse -->
    </div>
  </nav>

  <div class="container">
    <div class="starter-template">
      <h1>HUNTBAZAAR</h1>
      <p class="lead">Kami akan mengadakan HUNTBAZAAR, <br>daftarkan diri anda untuk mendapatkan harga terbaik dari kami.</p>
    </div>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- Include the plugin's CSS and JS: -->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" type="text/css" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>


    <form method="POST" action="" multiple="multiple">
      @csrf
      <div class="form-group">
        <label for="exampleInputEmail1">Alamat Email</label>
        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" value="{{$email}}" disabled>
      </div>
      <div class="form-group">
        <label for="">nama</label>
        <input type="text" class="form-control" name="nama">
      </div>
      <div class="form-group">
        <label for="">Jenis Kelamin</label>
        <select name="jk" class="form-control" required>
          <option value="">Pilih Jenis kelamin</option>
          <option value="Pria">Pria</option>
          <option value="Wanita">Wanita</option>
        </select>
      </div>
      <div class="form-group">
        <label for="">Tanggal Lahir</label>
        <input type="date" id="date" class="form-control" name="tanggalLahir">
      </div>

      <div class="form-group">
        <label for="">Pilih Designer Favorit</label><br>
        <select id="member" multiple="multiple" name="designer[]" required>
          @foreach($designer as $row)
          <option value="{{$row->nama}}">{{$row->nama}}</option>
          @endforeach
        </select>
      </div>
      <button type="submit" class="btn btn-primary" id="tombolDaftar">Daftarkan saya</button>
    </form>

    <!-- Initialize the plugin: -->
    <script type="text/javascript">
      $(document).ready(function() {
        $('#member').multiselect({
          enableClickableOptGroups: true,
          enableFiltering: true,
          enableCaseInsensitiveFiltering: true
        });
      });
    </script>

    <p id="demo"></p>
    <p id="MAX_DAFTAR" hidden>{{$MAX_DAFTAR}}</p>

    <script>
      // Set the date we're counting down to
      var max_daftar = document.getElementById('MAX_DAFTAR').innerHTML;
      max_daftar = new Date(max_daftar + "T00:00:00");

      var countDownDate = max_daftar;

      // Update the count down every 1 second
      var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        // Display the result in the element with id="demo"
        document.getElementById("demo").innerHTML = "sisa waktu untuk mendaftar<br>" + days + "Hari " + hours + "Jam " +
          minutes + "menit " + seconds + "detik ";

        // If the count down is finished, write some text
        if (distance < 0) {
          clearInterval(x);
          document.getElementById("demo").innerHTML = "Mohon maaf, anda terlambat mendaftar";
          //document.getElementById("tombolDaftar").setAttribute("disabled", "");

        }
      }, 1000);
    </script>



  </div>
</body>

</html>