<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/undangan', [App\Http\Controllers\UndanganController::class, 'index'])->name('undangan');
Route::POST('/undangan', [App\Http\Controllers\UndanganController::class, 'store'])->name('undangan');

Route::get('/registrasi/{email}/{link}', [App\Http\Controllers\UndanganController::class, 'registrasi'])->name('registrasi');
Route::POST('/registrasi/{email}/{link}', [App\Http\Controllers\UndanganController::class, 'registrasiSimpan'])->name('registrasi');

Route::get('/send-email', function () {

    $details['dest'] = $_GET['dest'];
    $details['body'] = $_GET['body'];;
    dispatch(new \App\Jobs\sendUndangan($details));
});

Route::get('test', function () {

    $details['dest'] = 'rifki@mailnesia.com';
    $details['body'] = 'rifki@mailnesia.com';
    dispatch(new \App\Jobs\SendEmailThankYou($details))->delay(2);
});
